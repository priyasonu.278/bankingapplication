package com.sabado.spring.exception;

@SuppressWarnings("serial")
public class NoMatchFoundException{
	private String errorMessage;
	private String errorCode;
	public NoMatchFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public NoMatchFoundException(String errorMessage, String errorCode) {
		super();
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	@Override
	public String toString() {
		return "NoMatchFoundException [errorMessage=" + errorMessage + ", errorCode=" + errorCode + "]";
	}
	
}
