package com.sabado.spring.service;

import java.util.List;

import com.sabado.spring.entity.BankLoan;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountLoanEntityResponse;

public interface BankLoanService {

	public List<AccountLoanEntityResponse> getAccountLoanEntity();
 

}
