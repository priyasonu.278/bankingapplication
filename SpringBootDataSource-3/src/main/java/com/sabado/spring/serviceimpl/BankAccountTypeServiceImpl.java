package com.sabado.spring.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sabado.spring.dao.BankAccountTypeDao;
import com.sabado.spring.entity.BankAccountType;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountTypesEntityResponse;
import com.sabado.spring.service.BankAccountTypeService;

@Service("bankaccounttypeservice")
public class BankAccountTypeServiceImpl implements BankAccountTypeService{
	
	@Autowired
	private BankAccountTypeDao bankaccounttypeDao;
	

	@Override
	public List<AccountTypesEntityResponse> getAccountType() {
		List<AccountTypesEntityResponse> accounttypes = bankaccounttypeDao.getAccountTypes();
		return accounttypes;
		
	}
	
	

}
