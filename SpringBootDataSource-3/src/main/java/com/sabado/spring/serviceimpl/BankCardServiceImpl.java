package com.sabado.spring.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sabado.spring.dao.BankBranchesDao;
import com.sabado.spring.dao.BankCardDao;
import com.sabado.spring.entityResponse.BankNameEntityResponse;
import com.sabado.spring.entityResponse.Bank_Id_Entity_Response;
import com.sabado.spring.service.BankBranchService;
import com.sabado.spring.service.BankCardService;
@Service("bankcardservice")
public class BankCardServiceImpl implements BankCardService {


	@Autowired
	private BankCardDao bankcardDao;

	@Override
	public List<Bank_Id_Entity_Response> getBankById() {
		List<Bank_Id_Entity_Response> bankid = bankcardDao.getBankId();
		return bankid;
		
	}
	

}
