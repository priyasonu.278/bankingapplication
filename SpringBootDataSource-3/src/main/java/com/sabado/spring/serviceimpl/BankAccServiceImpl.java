package com.sabado.spring.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sabado.spring.dao.BankAccDao;
import com.sabado.spring.entity.BankAccount;
import com.sabado.spring.entityResponse.AccountHolderEntityResponse;
import com.sabado.spring.entityResponse.BankEntityResponse;
import com.sabado.spring.entityResponse.BankNameEntityResponse;
import com.sabado.spring.service.BankAccService;

@Service("bankaccservice")
public class BankAccServiceImpl implements BankAccService{
	
	@Autowired
	private BankAccDao bankaccDao;

	@Override
	public List<BankEntityResponse> getBankEntity() {
		List<BankEntityResponse> bankentity = bankaccDao.getBankEntity();
		return bankentity;
		
	}





	
	

}
