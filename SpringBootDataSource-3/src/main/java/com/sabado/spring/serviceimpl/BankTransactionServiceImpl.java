package com.sabado.spring.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sabado.spring.dao.BankTransactionDao;
import com.sabado.spring.entity.BankTransaction;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.TransactionAmountEntityResponse;
import com.sabado.spring.service.BankTransactionService;


@Service("banktransactionservice")
public class BankTransactionServiceImpl implements BankTransactionService {
	
	@Autowired
	private BankTransactionDao banktransactiondao;

	


	@Override
	public List<AccountIdNameResponse> getAccountId() {
		List<AccountIdNameResponse> account = banktransactiondao.getAccountAmount();
		return account;
	}

	
	

}
