package com.sabado.spring.entityResponse;

import java.util.List;

public class Account_Id_Entity_Response {
	private int account_id;
	private String account_holder_name;
	private String account_type_name;
	private String status_name;
	private List<Card_Id_Entity_Response> card;
	public Account_Id_Entity_Response() {
		super();
	}
	public Account_Id_Entity_Response(int account_id, String account_holder_name, String account_type_name,
			String status_name, List<Card_Id_Entity_Response> card) {
		super();
		this.account_id = account_id;
		this.account_holder_name = account_holder_name;
		this.account_type_name = account_type_name;
		this.status_name = status_name;
		this.card = card;
	}
	public int getAccount_id() {
		return account_id;
	}
	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}
	public String getAccount_holder_name() {
		return account_holder_name;
	}
	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}
	public String getAccount_type_name() {
		return account_type_name;
	}
	public void setAccount_type_name(String account_type_name) {
		this.account_type_name = account_type_name;
	}
	public String getStatus_name() {
		return status_name;
	}
	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}
	public List<Card_Id_Entity_Response> getCard() {
		return card;
	}
	public void setCard(List<Card_Id_Entity_Response> card) {
		this.card = card;
	}
	

}
