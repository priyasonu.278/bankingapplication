package com.sabado.spring.entityResponse;

public class LoanTypeEntityResponse {

	private int loan_id;
	private int loan_type_id;
	private String loan_type_name;
	private int loan_amount;
	private int payment_amount;
	private int interest_paying;
	private String rate_of_interest;
	private String duration;
	public LoanTypeEntityResponse() {
		super();
	}
	public LoanTypeEntityResponse(int loan_id, int loan_type_id, String loan_type_name, int loan_amount,
			int payment_amount, int interest_paying, String rate_of_interest, String duration) {
		super();
		this.loan_id = loan_id;
		this.loan_type_id = loan_type_id;
		this.loan_type_name = loan_type_name;
		this.loan_amount = loan_amount;
		this.payment_amount = payment_amount;
		this.interest_paying = interest_paying;
		this.rate_of_interest = rate_of_interest;
		this.duration = duration;
	}
	public int getLoan_id() {
		return loan_id;
	}
	public void setLoan_id(int loan_id) {
		this.loan_id = loan_id;
	}
	public int getLoan_type_id() {
		return loan_type_id;
	}
	public void setLoan_type_id(int loan_type_id) {
		this.loan_type_id = loan_type_id;
	}
	public String getLoan_type_name() {
		return loan_type_name;
	}
	public void setLoan_type_name(String loan_type_name) {
		this.loan_type_name = loan_type_name;
	}
	public int getLoan_amount() {
		return loan_amount;
	}
	public void setLoan_amount(int loan_amount) {
		this.loan_amount = loan_amount;
	}
	public int getPayment_amount() {
		return payment_amount;
	}
	public void setPayment_amount(int payment_amount) {
		this.payment_amount = payment_amount;
	}
	public int getInterest_paying() {
		return interest_paying;
	}
	public void setInterest_paying(int interest_paying) {
		this.interest_paying = interest_paying;
	}
	public String getRate_of_interest() {
		return rate_of_interest;
	}
	public void setRate_of_interest(String rate_of_interest) {
		this.rate_of_interest = rate_of_interest;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	
	
	
}
