package com.sabado.spring.dao;

import java.util.List;
import com.sabado.spring.entity.BankLoan;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountLoanEntityResponse;
import com.sabado.spring.entityResponse.LoanTypeEntityResponse;
import com.sabado.spring.entityResponse.TransactionAmountEntityResponse;

public interface BankLoanDao {
	
	public List<AccountLoanEntityResponse> getAccountLoan();
	
	
	 public List<LoanTypeEntityResponse> getLoanType(int account_id);
	

}
