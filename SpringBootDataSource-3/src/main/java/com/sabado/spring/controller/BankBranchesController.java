package com.sabado.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sabado.spring.entityResponse.BankNameEntityResponse;
import com.sabado.spring.service.BankBranchService;
@CrossOrigin()
@RestController
public class BankBranchesController {
	
	
	@Autowired
	
    public BankBranchService bankbranchesservices;

    @RequestMapping("/bankbranches")
 
    public List<BankNameEntityResponse> getBankBranch() {
        List<BankNameEntityResponse> banks = bankbranchesservices.getBankNamesById();
        return banks;
    }
   
}
