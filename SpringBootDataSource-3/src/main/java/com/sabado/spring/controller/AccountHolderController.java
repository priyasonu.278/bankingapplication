package com.sabado.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sabado.spring.entity.AccountHolder;
import com.sabado.spring.entity.Bank;
import com.sabado.spring.exception.NoMatchFoundException;
import com.sabado.spring.service.AccountHolderService;


@RestController   //to create RESTFUL web service of  account_holder_controller
public class AccountHolderController {

	@Autowired 
	 private AccountHolderService accountholderservice;
	 
	 @RequestMapping(value = "/holders", method = RequestMethod.GET, produces = "application/json")
	 public ResponseEntity<List<AccountHolder>> holders() {
	 
	  HttpHeaders headers = new HttpHeaders();
	  List<AccountHolder> holders = accountholderservice.getAccountholder();
	 
	  if (holders == null) {
	   return new ResponseEntity<List<AccountHolder>>(HttpStatus.NOT_FOUND);
	  }
	  headers.add("Number Of Records Found", String.valueOf(holders.size()));
	  return new ResponseEntity<List<AccountHolder>>(holders, headers, HttpStatus.OK);
	 }
	 
	 @RequestMapping(value = "/holders/{account_holder_id}", method = RequestMethod.GET)
	 public ResponseEntity<AccountHolder> getAccountHolder(@PathVariable("account_holder_id") int account_holder_id) {
	 AccountHolder accountholder = accountholderservice.getAccount(account_holder_id);
	  if (accountholder == null) {
	   return new ResponseEntity<AccountHolder>(HttpStatus.NOT_FOUND);
	  }
	  return new ResponseEntity<AccountHolder>(accountholder, HttpStatus.OK);
	 }
	 
	 
	 @RequestMapping(value = "/accountholder", method = RequestMethod.POST,produces = "application/json")
	 public ResponseEntity<AccountHolder> createAccountHolder(@RequestBody AccountHolder accountholder) {
	 HttpHeaders headers = new HttpHeaders();
	
		 if (accountholder == null) {
		   return new ResponseEntity<AccountHolder>(HttpStatus.BAD_REQUEST);
		  }
		 accountholderservice.createAccount(accountholder);
		  headers.add("AccountHolder Created  - ", String.valueOf(accountholder.getAccount_holder_id()));
		  return new ResponseEntity<AccountHolder>(accountholder, headers, HttpStatus.CREATED);
		 }
	 
	 
	 
	 @RequestMapping(value = "/updateholder/{account_holder_id}", method = RequestMethod.PUT)
	 public ResponseEntity<AccountHolder> updateAccountHolder(@PathVariable("account_holder_id") int account_holder_id, @RequestBody AccountHolder accountholder) {
	  HttpHeaders headers = new HttpHeaders();
	  AccountHolder isExist = accountholderservice.getAccount(account_holder_id);
	  if (isExist == null) {   
	   return new ResponseEntity<AccountHolder>(HttpStatus.NOT_FOUND);
	  } else if (accountholder == null) {
	   return new ResponseEntity<AccountHolder>(HttpStatus.BAD_REQUEST);
	  }
	  accountholderservice.updateAccount(accountholder);
	  headers.add("accountholder Updated  - ", String.valueOf(account_holder_id));
	  return new ResponseEntity<AccountHolder>(accountholder, headers, HttpStatus.OK);
	 }
	 
}
