package com.sabado.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sabado.spring.entity.BankLoan;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountLoanEntityResponse;
import com.sabado.spring.service.BankLoanService;

@RestController
public class BankLoanController {
	
	@Autowired
    public BankLoanService getloans;

    @RequestMapping("/bankloan")
    public List<AccountLoanEntityResponse> getAccountLoan() {
        List<AccountLoanEntityResponse> bankloan = getloans.getAccountLoanEntity();
		return bankloan;
		
		
        
        
    }

}
