package com.sabado.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sabado.spring.entity.BankTransaction;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.TransactionAmountEntityResponse;
import com.sabado.spring.service.BankTransactionService;

@RestController
public class BankTransactionController {
	
	@Autowired
    public BankTransactionService banktransactionservice;

    @RequestMapping("/banktransaction")
    public List<AccountIdNameResponse> getAccounts() {
        List<AccountIdNameResponse> banktransaction = banktransactionservice.getAccountId();
		return banktransaction;
		
        
        
    }
   
 	
	
	
	

}
