package com.sabado.spring.entity;

public class BankBranches {
	
	  private int bank_id;
	    private String bank_name;
	    private String branch_name ;
	    
	    
		public BankBranches() {
			super();
		}


		public BankBranches(int bank_id, String bank_name, String branch_name) {
			super();
			this.bank_id = bank_id;
			this.bank_name = bank_name;
			this.branch_name = branch_name;
		}


		public int getBank_id() {
			return bank_id;
		}


		public void setBank_id(int bank_id) {
			this.bank_id = bank_id;
		}


		public String getBank_name() {
			return bank_name;
		}


		public void setBank_name(String bank_name) {
			this.bank_name = bank_name;
		}


		public String getBranch_name() {
			return branch_name;
		}


		public void setBranch_name(String branch_name) {
			this.branch_name = branch_name;
		}
	    
	    
	    
	    
	    
	    
	

}
